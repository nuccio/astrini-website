---
title: About  
layout: nobread_page 
---

Version 0.1, January 2015

* Idea: **Roberto Casati**
* Design: **Roberto Casati, Glen Lomax**
* Code: **Glen Lomax**



References 
==========

Casati, R., 2013, Dov'e il Sole di notte? Lezioni atipiche di astronomia. Milano: Raffaello Cortina.

Licence and copyright policy
----------------------------

Astrini's first version was designed during an internship at the CogMaster (ENS-EHESS-Paris5). The current version is distributed freely under the [GPL](http://www.gnu.org/licenses/agpl-3.0.txt). Roberto Casati and Glen Lomax are the original developers.

Donations
---------

Even small donations keep volunteer work afloat!
(no donation platform yet)


Foundations
===========

Astrini is coded in the [Python language](https://www.python.org/).

We include the [Astronomia](https://pypi.python.org/pypi/astronomia/) python module for future ephemeris precison improvements. 

The 3D python engine supporting astrini is [Panda3D](https://www.panda3d.org/), very nice for prototyping and seems robust.
